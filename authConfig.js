'use strict';

module.exports = authenticateRoute;
var excludedRoutes = ['/login', '/register'];
var _ = require('lodash');

function authenticateRoute(req, res, next) {
    var isExcluded = _.includes(excludedRoutes, req.url);

    if (!isExcluded)
        return authenticate(req, res, next);
    
    next();
}

function authenticate(req, res, next) {
    if(!req.user)
        return res.redirect('/login');

    next();
}
