'use strict';

var onAppReadyObservers = [];
//example hook ups for app ready
//var socketService = require('./services/socketService.js');
//onAppReadyObservers.push(socketService.subscribeToSensorChange);

module.exports = {
    subscribeToAppReady:subscribeToAppReady,
    triggerAppReady: triggerAppReady
};

function subscribeToAppReady(observerFunc){
    onAppReadyObservers.push(observerFunc);
}

function triggerAppReady(){
    var args = arguments;
    onAppReadyObservers.forEach((func) => {
        func.call(this, args);
    });
}