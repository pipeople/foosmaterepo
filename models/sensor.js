const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    SensorModel = {
        value: Number,
        type: String, //TODO: transform to FK?
        description: String
    },
    SensorSchema = new Schema(SensorModel);

module.exports = mongoose.model('Sensor', SensorSchema);
