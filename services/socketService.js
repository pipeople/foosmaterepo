'use strict';

let itemService = require('../services/itemService.js'),
    selfIO;

module.exports = {
    init: init,
};

function handleSensorUpdate(sensor) {
    itemService.getItems({ 'sensorState.id': sensor.sensorId })
        .then(updateClient);
}

function updateClient(data){
    let room = data[0].companyId;

    selfIO.sockets.in(room).emit('itemStateChanged', data);
    console.log("emitting to room " + room);
}

function init(io){
    selfIO = io;
    hookOnClientConnected(io);
    itemService.subscribeToSensorChange(handleSensorUpdate);
}

function hookOnClientConnected(io){
    io.sockets.on('connection', function (socket) {
        socket.on("joinRoom", function(room){
            socket.join(room);
        });
    });
}
