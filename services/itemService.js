'use strict';

let _ = require('lodash'),
    mongoose = require('mongoose'),
    Item = mongoose.model('Item'),
    sensorChangeObservers = [],
    crudOperations = require('../utilities/generateCrudOperations.js')(Item),
    itemService = {};

itemService.create = crudOperations.create;
itemService.edit = crudOperations.edit;
itemService.delete = crudOperations.delete;
itemService.getItems = getItems;
itemService.updateSensorStateAndEmit = updateSensorStateAndEmit;
itemService.subscribeToSensorChange = sensorChangeObservers.push.bind(sensorChangeObservers);

module.exports = itemService;

function getItems(query, cancelCompute) {
    let items = Item.find(query || {})
        .lean()
        .exec();

    if(!cancelCompute)
        items.then(computeItemsStatus);

    return items;
}

// implement events using EventEmitter
function updateSensorStateAndEmit(sensorId, value){
    return updateSensorState(sensorId, value)
        .then(triggerSensorChange);
}

function updateSensorState(sensorId, value) {
    let bySensorId = { 'sensorState.id': sensorId },
        update = { $set: { 'sensorState.$.value': value} };

    return Item.findOneAndUpdate(bySensorId, update)
        .exec()
        .then(function() {
            return { sensorId: sensorId, value: value };
        }, function(err) {
            console.log("Could not update sensor" + sensorId);
        });
}


function triggerSensorChange() {
    let args = arguments;
    sensorChangeObservers.forEach((func) => {
        func.apply(this, args);
    });
}

function computeItemsStatus(items) {
    _.forEach(items, setStatus);
}

// this needs some revision to be able to handle more than binary states
function setStatus(item) {
    let status = _.every(item.sensorState, { value: false });

    item.status = status ? "Free" : "Busy";
}
