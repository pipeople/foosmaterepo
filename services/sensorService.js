'use strict';

let _ = require('lodash'),
    mongoose = require('mongoose'),
    Sensor = mongoose.model('Sensor'),
    crudOperations = require('../utilities/generateCrudOperations.js')(Sensor),
    sensorService = {};

sensorService.create = crudOperations.create;
sensorService.edit = crudOperations.edit;
sensorService.delete = crudOperations.delete;
sensorService.getSensors = getSensors;

module.exports = sensorService;


function getSensors(query) {
    let sensors = Sensor.find(query || {})
        .lean()
        .exec();
    return sensors;
}
