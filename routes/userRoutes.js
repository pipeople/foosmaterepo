'use strict';

module.exports = function indexRoutes(app) {
    app.get('/user/getCompany', function (req, res) {
        if(req.user){
            res.send(req.user._doc.companyId);
        }
        else{
            res.send("You must be logged in.");
        }
    });
};