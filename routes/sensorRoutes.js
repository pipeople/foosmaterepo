'use strict';

let sensorService = require('../services/sensorService.js'),
    c = require("bluebird").coroutine;

module.exports = function receiverRoutes(app) {

    app.get('/sensor/all', c(function *(req, res) {
        let sensors = yield sensorService.getSensors();
        res.send(sensors);
    }));

    app.post('/sensor/create', c(function *(req, res) {
        let sensorData = req.body;

        let sensor = yield sensorService.create(sensorData);
        
        res.send(200);
    }));

    app.post('/sensor/edit', c(function *(req, res) {
        let sensorData = req.body;

        let sensor = yield sensorService.edit(sensorData);
        
        res.send(200);
    }));

    app.post('/sensor/delete', c(function *(req, res) {
        let sensorId = req.body._id;

        let sensor = yield sensorService.delete(sensorId);
        
        res.send(200);
    }));
};
