'use strict';

let passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = function loginRoutes(app) {
    app.get('/login', (req, res) => {
        res.render('login');
    });

    app.post('/login', passport.authenticate('local'), (req, res) => {
        res.redirect('index');
    });

    app.get('/register', (req, res) => {
        res.render('register');
    });

    app.post('/register', (req, res) => {
        let user = new User({ username: req.body.username, companyId: "Company1" });
        
        User.register(user, 
            req.body.password, 
            (err, account) => {
                if(err) {
                    return res.send(JSON.stringify(err));
                }

                passport.authenticate('local')(req, res, () => {
                    res.redirect('/login');
                });
            });
    });
};
