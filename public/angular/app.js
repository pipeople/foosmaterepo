(function() {
    'use strict';
    
    var appModules = ['dashboard','item', 'socket', 'company', 'util', 'common', 'constants', 'sensor', 'ui.router'],
        app = angular.module('myApp', appModules)
            .run(initModules);    

    function initModules() {}
})();
