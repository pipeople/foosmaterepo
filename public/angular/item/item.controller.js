(function() {
    'use strict';
    //TODO: show only for admins
    angular.module('item', [])
        .controller('ItemCtrl', ItemCtrl);

    ItemCtrl.$inject = ['$scope', 'ItemService', 'Constant', 'SensorService'];

    function ItemCtrl($scope, ItemService, Constant, SensorService) {
        var self = this;
        self.submitItem = submitItem;
        self.updateItem = updateItem;
        self.deleteItem = deleteItem;
        self.items = [];
        self.itemTypes = Constant.ItemTypes;
        self.defaultType = self.itemTypes[0];

        populateItems();
        populateSensors();
        
        function submitItem(item) {
            ItemService.createItem(item);
        }

        function updateItem(item){
            ItemService.editItem(item);
        }

        function deleteItem(id){
            ItemService.deleteItem(id);
        }

        function populateItems(){
            ItemService.getItems().then(function(result){
                self.items = result.data;
            });
        }

        function populateSensors(){
            SensorService.getSensors().then(function(result){
                self.sensors = result.data;
            });
        }
    }
})();