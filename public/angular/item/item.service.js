(function() {    
    'use strict';

    angular.module('item')
        .factory('ItemService', ItemService);

    ItemService.$inject = ['$http'];
    function ItemService($http) {
        var service = {};

        service.getItems = getItems;
        service.createItem = createItem;
        service.editItem = editItem;;
        service.deleteItem = deleteItem;

        return service;
        
        function getItems() {
            return $http({
                method: 'GET',
                url: 'http://localhost:3000/item/all',
           });
        }

        function createItem(item) {
            return $http({
                method: 'POST',
                url: 'http://localhost:3000/item/create',
                data: item
            });
        }
        function editItem(item){
            return $http({
                method: 'POST',
                url: 'http://localhost:3000/item/edit',
                data: item
            });
        }
        function deleteItem(item){
            return $({
                method: 'POST',
                url: 'http://localhost:3000/item/delete',
                data: item
            });
        }
    }
})();
