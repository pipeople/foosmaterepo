(function() {   
    "use strict" 
    angular.module('item')
     .directive('item', function(){
        return {
            restrict: 'E',
            scope:{
                items: '='
            },
            templateUrl: "/directiveTemplates/item.html",
            controller: ItemDirectiveController
        }
    });

    ItemDirectiveController.$inject = ['$scope', 'ConstantsService'];

    function ItemDirectiveController($scope, constants) {
        var vm = this;
        console.log(constants.ITEM_BACKGROUND_COLORS);
    }
})();