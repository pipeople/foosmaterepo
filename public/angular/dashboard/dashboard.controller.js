(function() {
    'use strict';

    angular.module('dashboard', [])
        .controller('DashboardCtrl', DashboardCtrl);

        DashboardCtrl.$inject = ['$scope', '$q', 'ItemService', 'SocketService', 'CompanyService', 'RequestUtils'];
        function DashboardCtrl($scope, $q, ItemService, SocketService, CompanyService, RequestUtils) {

            var items = ItemService.getItems(),
                company = CompanyService.getCompanyId();

            // after we get both items and companies
            $q.all([items, company])
                .then(function(data) {
                    var items = $scope.items = data[0].data,
                        companyId = data[1].data;

                    socketConnect(companyId, items);
                },
                RequestUtils.logError);

            function socketConnect(companyId){
                SocketService.emit("joinRoom", companyId);
                SocketService.on('itemStateChanged', function(data) {
                    if(data && data[0])
                        var updatedItem = _.find($scope.items, { _id: data._id });
                        updatedItem.status = data.status;
                });
            }
        }
})();
