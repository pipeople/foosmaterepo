(function() {   
    "use strict" 
    angular.module('dashboard')
     .directive('dashboard', function(){
        return {
            restrict: 'E',
            scope:{
                items: '='
            },
            templateUrl: "/directiveTemplates/dashboard.html",
            controller: DashboardDirectiveController
        }
    });

    DashboardDirectiveController.$inject = ['$scope'];

    function DashboardDirectiveController($scope) {
        var vm = this;
    }
})();
