(function() {    
    'use strict';

    angular.module('constants', [])
        .factory('ConstantsService', ConstantsService);

    ConstantsService.$inject = [];
    function ConstantsService() {
        var constants = {};

        constants.APP_NAME = 'FOOSMATE';
        constants.DEFAULT_URL = '/';

        constants.MENU_LINKS = [
            {
                "name": 'Register',
                "url": '/register'
            },
            {
                "name": 'Login',
                "url": '/login'
            },
            {
                "name": 'Dashboard',
                "url": '/index#/dashboard'
            },
            {
                "name": 'Contact',
                "url": '/contact'
            },
            {   //TODO: show only for admins
                "name": 'Create Item',
                "url" : '#/create-item'
            },
            {
                "name": 'Edit Item',
                "url" : '#/edit-item'
            },
            {
                "name": 'Create Sensor',
                "url" : '#/create-sensor'
            },
            {
                "name": 'Edit Sensor',
                "url" : '#/edit-sensor'
            }
        ];

        constants.ITEM_BACKGROUND_COLORS = [
            '#f5e9d0',
            '#f0b338',
            '#25941b',
            '#1a8ea3',
            '#06ccab',
            '#9d4bad'
        ];

        return constants;
    }
})();