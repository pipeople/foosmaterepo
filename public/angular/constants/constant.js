(function(){
    'use strict'
    angular.module('constants')
    .constant('Constant', {
        SERVERURL: 'http://localhost:3000',
        SensorTypes: ['Infrared', 'Pressure', 'Termo'],
        ItemTypes: ['Room', 'Foosball', 'Electronic']
    });
})();