(function() {    
    'use strict';

    angular.module('util', [])
        .factory('RequestUtils', RequestUtils);

    RequestUtils.$inject = ['$http'];
    function RequestUtils($http) {
        var service = {};

        service.logError = logError;

        return service;
        
        function logError(err) { console.log(err); }
    }
})();
