(function () {
    'use strict';

    angular.module('myApp')
        .config(function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('dashboard');

            $stateProvider
                .state('dashboard', {
                    url: '/dashboard',
                    templateUrl: 'templates/dashboard',
                    controller: 'DashboardCtrl',
                    controllerAs: 'dashboardCtrl'
                })
                .state('create-item', {
                    url: '/create-item',
                    templateUrl: 'templates/createItem',
                    controller: 'ItemCtrl',
                    controllerAs: 'itemCtrl'
                })
                .state('edit-item', {
                    url: '/edit-item',
                    templateUrl: 'templates/editItem',
                    controller: 'ItemCtrl',
                    controllerAs: 'itemCtrl'
                })
                .state('create-sensor', {
                    url:'/create-sensor',
                    templateUrl: 'templates/createSensor',
                    controller: 'SensorCtrl',
                    controllerAs: 'sensorCtrl'
                })
                .state('edit-sensor', {
                    url:'/edit-sensor',
                    templateUrl: 'templates/editSensor',
                    controller: 'SensorCtrl',
                    controllerAs: 'sensorCtrl'
                });
        });
})();