(function(){
    'user strict'
    angular.module('sensor')
    .factory('SensorService', SensorService);

    SensorService.$inject = ['$http', 'Constant'];
    
    function SensorService($http, Constant){
        var service = {};

        service.getSensors = getSensors;
        service.createSensor = createSensor;
        service.editSensor = editSensor;
        service.deleteSensor = deleteSensor;

        return service;

        function getSensors(){
            return $http({
                method: 'GET',
                url: Constant.SERVERURL + '/sensor/all'
            });
        }

        function createSensor(sensor){
            return $http({
                method: 'POST',
                url: Constant.SERVERURL + '/sensor/create',
                data: sensor
            });
        }

        function editSensor(sensor){
            return $http({
                method: 'POST',
                url: Constant.SERVERURL + '/sensor/edit',
                data: sensor
            });
        }

        function deleteSensor(sensor){
            return $http({
                method: 'POST',
                url: Constant.SERVERURL + '/sensor/delete',
                data: sensor
            });
        }
    }
})();