'use strict';

const express = require('express'),
    fs = require('fs'),
    passport = require('passport'),
    configRoutes = require('./routes/routesConfig.js').loadRoutes,
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    authenticateRoute = require('./authConfig.js'),
    app = express(),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    accessLogStream = fs.createWriteStream(__dirname + '/httpLog.log', {flags: 'a'}),
    User = require('./models/user.js'),
    Item = require('./models/item.js'),
    appEvents = require('./applicationEvents.js'),
    io = require('socket.io').listen(app.listen(3000));

// var itemOne = new Item({
//     name: "Bathroom",
//     companyId: "CompanyOne",
//     sensorState: [
//         { id: "sensor1id", value: true }, { id: "sensor2id", value: true }
//     ]
// });

// var itemTwo = new Item({
//     name: "Djaga",
//     companyId: "CompanyTwo",
//     sensorState: [
//         { id: "sensor3id", value: true }, { id: "sensor4id", value: true }
//     ]
// });
 
// itemOne.save(function(){});
// itemTwo.save(function(){});

app.set('views', './views');
app.set('view engine', 'pug');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(session({ secret: 'what cat' }));
app.use(passport.initialize());
app.use(passport.session());

app.use(authenticateRoute);

app.use(morgan("combined", { timestamp: true, stream: accessLogStream }));

mongoose.connect('mongodb://localhost/test');
mongoose.Promise = global.Promise = require('bluebird');

passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

configRoutes(app);
appEvents.triggerAppReady();

require('./services/socketService.js').init(io);

console.log('ready and listening');
